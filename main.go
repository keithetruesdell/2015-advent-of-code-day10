package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

const (
	input string = "input/input.txt"
	ex1   string = "input/ex1.txt"
)

var (
	answer int
)

func main() {

	f2Use := getFlags()
	f2r := file2Read(f2Use)
	data, err := readFile(f2r)
	if err != nil {
		fmt.Printf("ERROR reading file:\n    %v\n", err)
	}

	// there should only be 1 row in the input file, don't care about the rest
	fmtD := fmtData(data[0])

	fmt.Println(fmtD)

	fmt.Println("======================================")
	fmt.Printf("Answer:  %v\n", answer)
}

// fmtData -
func fmtData(data string) (fmtD []string) {
	for i := 0; i < len(data); i++ {
		fmt.Println(data[i])
		fmtD = append(fmtD, data[i])
	}
	return fmtD
}

// readFile -
func readFile(f2r string) (data []string, err error) {
	inpF, err := os.Open(f2r)
	if err != nil {
		return data, err
	}

	defer inpF.Close()

	scn := *bufio.NewScanner(inpF)

	for scn.Scan() {
		ln := strings.TrimSpace(scn.Text())
		data = append(data, ln)
	}

	return data, err
}

// file2Read -
func file2Read(f2u int) (f2r string) {
	switch f2u {
	case 0:
		return input
	case 1:
		return ex1
	default:
		return ex1
	}
}

// getFlags -
func getFlags() (ex int) {
	tmpEx := flag.Int("e", 1, "what example file to use?  1 is the default, 0 is production")
	flag.Parse()
	ex = *tmpEx
	return ex
}
